# G-STORE v 1.0 #

G Store es una aplicación web para una tienda en línea de venta de diversos productos. Permite la visualización, pago y seguimiento de envíos por parte de los clientes y control de inventarios, seguimiento de ventas y servicio al cliente por parte del negocio.

### Principales Caracteristicas ###

* Para clientes
    * Catálogo de productos por tipo y categoría
    * Pago en línea a través de una plataforma de pagos
    * Creación de cuentas
    * Seguimiento de envíos
    * Whishlist
    * Carrito de compras
* Para el negocio
    * Control de inventarios
    * Reportería
    * Seguimiento a envíos
    * Control de productos

### Principales Tecnologías de desarrollo ###

* HTML
* CSS
* JavaScript
* MySQL
* PHP
* Laravel
* React JS